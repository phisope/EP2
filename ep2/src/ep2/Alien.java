package ep2;

import javax.swing.ImageIcon;

public class Alien {
	
	private ImageIcon imagem;
	private int x, y;
	private boolean isVisivel;
	private int height, width;
	
	private static final int larguraTela = Game.getWidth();
	private static final int speed = 1;
	
	public Alien(int x, int y){
		
		this.x = x;
		this.y = y;
		
		ImageIcon image = new ImageIcon("images/alien.jpg");
		imagem = getImageIcon();
		
		this.height = imagem.getHeight(null);
		this.width = imagem.getIconWidth(null);
		
		isVisivel = true;
	}

	
	

	
}
