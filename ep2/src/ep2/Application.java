package ep2;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;


@SuppressWarnings("serial")
public class Application extends JFrame {
    
    public Application() {
    	
    	JMenuBar barMenu = new JMenuBar();
    	
    	JMenu menu = new JMenu("Menu");
    	
    	JMenuItem sobre = new JMenuItem("Sobre");
    	    	
    	JMenuItem sair = new JMenuItem("Sair");

        add(new Map());

        setSize(Game.getWidth(), Game.getHeight());

        setTitle("Space Combat Game");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
    
    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                Application app = new Application();
                app.setVisible(true);
            }
        });
    }
}